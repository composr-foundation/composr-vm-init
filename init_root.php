<?php

require('./libs/lib.php');

init();

$is_virtualbox = is_virtualbox();

$options = getopt('', array('hostname:', 'mysql-root-password:', 'mysql-user-password:', 'user-password:', 'verbose', 'git-email:', 'ssh-public-key:'));
if ($options === false) {
    exit('Usage: php ' . $_SERVER['argv'][0] . " --hostname=<hostname> --mysql-root-password=<mysql-root-password> --mysql-user-password=<mysql-user-password> --user-password=<user-password> --git-email=<git-email> --ssh-public-key=\"<ssh-public-key>\"\n");
}

$hostname = $options['hostname'];

$mysql_root_password = $options['mysql-root-password'];
$mysql_user_password = $options['mysql-user-password'];

$user_password = $options['user-password'];

$git_email = $options['git-email'];

$ssh_public_key = $options['ssh-public-key'];

$step_by_step = array_key_exists('verbose', $options);

$root_commands = array(
    // Set basic networking
    'hostnamectl set-hostname ' . $hostname,

    // Generate key and allow remote access
    'ssh-keygen -q -N "" -f /root/.ssh/id_rsa <<<y',
    array('APPEND_OR_CREATE', '/root/.ssh/authorized_keys', $ssh_public_key),
    'chmod 600 /root/.ssh/authorized_keys',
    'chmod 600 /root/.ssh/id_rsa',

    // Remove unneeded default packages
    'yum -y erase firewalld',

    // Disable selinux, it's a pain
    'setenforce 0',
    'sed -i s/^SELINUX=.*$/SELINUX=disabled/ /etc/selinux/config',

    // System update
    'yum -y update',

    // Install some packages we want as a sysadmin
    'yum -y install rsync tar gzip zip wget bc net-tools nfs-utils psmisc sysstat less man iptraf-ng chrony bind-utils yum-utils',

    // EPEL
    'yum -y install epel-release',
    'yum -y install subscription-manager',
    'dnf config-manager --set-enabled powertools',

    // Remi PHP, for newer PHP version
    'yum install -y https://rpms.remirepo.net/enterprise/remi-release-8.5.rpm',
    'dnf -y module reset php',
    'dnf -y module install php:remi-8.1',

    // Automatic updates
    'yum -y install dnf-automatic',
    array('REPLACE', '/etc/dnf/automatic.conf', 'apply_updates = no', 'apply_updates = yes'),
    'sudo systemctl enable --now dnf-automatic.timer',

    // NTP
    'systemctl enable chronyd.service',
    'systemctl restart chronyd.service',

    // Packages preferred by Composr
    'yum -y install httpd mod_ssl mod_fcgid php php-fpm php-common php-cli php-pecl-apcu php-pdo curl enchant freetype php-gd php-imap php-pecl-zip php-mysqlnd php-mbstring php-pspell php-xml php-intl php-json openssl mariadb-server mariadb hunspell hunspell-en aspell aspell-en poppler-utils',
    'yum -y install nano git openssh-server iotop screen zip unzip bzip2 whois lsof mlocate', // Useful for Health Checks and admin

    // Configure/Start MariaDB
    'systemctl start mariadb',
    "mysql --user=root <<_EOF_
UPDATE mysql.user SET Password=PASSWORD('{$mysql_root_password}') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
_EOF_
",
    array('CREATE', '/root/.my.cnf', "[client]
user=root
password={$mysql_root_password}
"),
    'chmod 600 /root/.my.cnf',
    "mysql <<_EOF_
CREATE USER 'cms'@'localhost' IDENTIFIED BY '{$mysql_user_password}';
CREATE DATABASE cms;
GRANT ALL PRIVILEGES ON cms.* TO 'cms'@'localhost';
FLUSH PRIVILEGES;
_EOF_
",
    array('APPEND', '/root/.my.cnf', 'database=cms'),
    array('APPEND_AFTER', '/etc/my.cnf.d/mariadb-server.cnf', '[server]', 'wait_timeout = 600'),
    array('APPEND_AFTER', '/etc/my.cnf.d/mariadb-server.cnf', '[server]', 'max_allowed_packet = 64M'),
    'systemctl enable mariadb',
    'systemctl restart mariadb',

    // Add cms user
    'useradd cms',
    'echo ' . escapeshellarg($user_password) . ' | passwd cms --stdin',
    'usermod -aG wheel cms',
    'usermod -aG apache cms',

    // Configure Apache
    'rm -f /etc/httpd/conf.d/welcome.conf', // Annoying default messages even for access denied or 404
    array('REPLACE', '/etc/httpd/conf.modules.d/00-mpm.conf', "\nLoadModule", "\n#LoadModule"),
    array('REPLACE', '/etc/httpd/conf.modules.d/00-mpm.conf', '#LoadModule mpm_event_module', 'LoadModule mpm_event_module'),
    'chown -R cms:cms /var/www/html',
    array('PREG_REPLACE', '/etc/httpd/conf/httpd.conf', '#(<Directory "/var/www/html">(.|\n)*?)AllowOverride None#', '$1AllowOverride All'),
    array('REPLACE', '/etc/httpd/conf/httpd.conf', 'User apache', 'User cms'),
    array('REPLACE', '/etc/httpd/conf/httpd.conf', 'Group apache', 'Group cms'),
    array('APPEND', '/etc/httpd/conf/httpd.conf', 'RewriteMap escape int:escape'),
    array('APPEND', '/etc/httpd/conf/httpd.conf', "RemoteIPHeader X-Forwarded-For\nRemoteIPInternalProxy 192.168.200.0/24"),

    // Configure PHP
    array('REPLACE', '/etc/php.ini', ';cgi.fix_pathinfo=1', 'cgi.fix_pathinfo=0'),
    array('REPLACE', '/etc/php.ini', 'post_max_size = 8M', 'post_max_size = 80M'),

    // Configure PHP FPM
    array('REPLACE', '/etc/php-fpm.d/www.conf', '= nobody', '= cms', true),
    array('REPLACE', '/etc/php-fpm.d/www.conf', '= apache', '= cms', true),
    array('REPLACE', '/etc/php-fpm.d/www.conf', ';listen.owner', 'listen.owner'),
    array('REPLACE', '/etc/php-fpm.d/www.conf', ';listen.group', 'listen.group'),
    array('REPLACE', '/etc/httpd/conf.modules.d/20-php.conf', 'LoadModule', '#LoadModule'),
    array('APPEND', '/etc/httpd/conf.d/php.conf', "<Proxy \"unix:/run/php-fpm/www.sock|fcgi://php-fpm\">\nProxySet disablereuse=off\n</Proxy>"),
    array('APPEND', '/etc/httpd/conf.d/php.conf', "<FilesMatch \.php$>\nSetHandler proxy:fcgi://php-fpm\n</FilesMatch>"),

    // Start Apache + PHP
    'systemctl enable php-fpm',
    'systemctl restart php-fpm',
    'systemctl enable httpd',
    'systemctl restart httpd',
);

process_commands($root_commands, $step_by_step);
