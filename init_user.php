<?php

require('./libs/lib.php');

init();

$options = getopt('', array('mysql-user-password:', 'verbose', 'repos:', 'git-email:', 'ssh-public-key:'));
if ($options === false) {
    exit('Usage: php ' . $_SERVER['argv'][0] . " --mysql-user-password=<mysql-user-password> --repos=<git-repository> --git-email=<git-email> --ssh-public-key=\"<ssh-public-key>\"\n");
}

$mysql_user_password = $options['mysql-user-password'];
$repos = $options['repos'];

$git_email = $options['git-email'];

$ssh_public_key = $options['ssh-public-key'];

$step_by_step = array_key_exists('verbose', $options);

if (!file_exists('/home/cms')) {
    exit('cms user not yet created');
}

$cms_commands = array(
    // Generate key and allow remote access
    'ssh-keygen -q -N "" -f /home/cms/.ssh/id_rsa <<<y',
    array('APPEND_OR_CREATE', '/home/cms/.ssh/authorized_keys', $ssh_public_key),
    'chmod 600 /home/cms/.ssh/authorized_keys',
    'chmod 600 /home/cms/.ssh/id_rsa',

    // Easy access to webroot
    'ln -s /var/www/html /home/cms/www',

    // MySQL
    array('CREATE', '/home/cms/.my.cnf', "[client]
user=cms
password={$mysql_user_password}
database=cms
"),
    'chmod 600 /home/cms/.my.cnf',
);

process_commands($cms_commands, $step_by_step);

echo "ACTION REQUIRED:\n";
echo "(Unless the repository is public) Add this key as a deploy key (with write permissions) on your GitLab repository...\n";
echo file_get_contents('/home/cms/.ssh/id_rsa.pub');

echo "(Press enter when you have done the above)\n";
$stdin = fopen('php://stdin', 'r');
flush();
fgets($stdin);
fclose($stdin);

// Set up Composr CMS website...

chdir('/var/www/html');

$cms_commands = array(
    'ssh-keyscan gitlab.com >> /home/cms/.ssh/known_hosts',
    'git clone ' . $repos . ' /var/www/html',
    'find /var/www/html -type d -exec chmod 755 {} \; ; find . -type f -exec chmod 644 {} \;',
    "sed -i 's/utf8mb4_0900_ai_ci/utf8mb4_general_ci/g' ~/db.sql", // Needed if MySQL versions do not match up well
    'mysql < ~/db.sql', // Just putting in a DB dump for now, but replication is what we really want

    // Configure Git
    'git config core.fileMode false',
    'git config --global push.default simple',
    'git config --global user.email "' . $git_email . '"',
    'git config --global user.name "' . gethostname() . '"',

    // Git pull hooks
    array('CREATE', '/var/www/html/.git/hooks/post-merge', "#!/bin/sh\n\ncd `git rev-parse --show-toplevel`\n\nsh php decache.php"),
    'chmod 755 /var/www/html/.git/hooks/post-merge',
);

process_commands($cms_commands, $step_by_step);

if (strpos(gethostname(), 'staging') === false) {
    // Live server...

    $cms_commands = array(
        // Set up Cron
        '(echo "* * * * * php /var/www/html/data/cron_bridge.php") | crontab -',
    );

    process_commands($cms_commands, $step_by_step);
} else {
    // Staging server...

    $cms_commands = array(
        'git checkout staging',
    );

    process_commands($cms_commands, $step_by_step);
}
