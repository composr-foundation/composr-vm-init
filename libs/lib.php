<?php

function init()
{
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    if (php_sapi_name() != 'cli') {
        header('Content-Type: text/plain');
        exit("Must be run on the command line\n");
    }

    if (strpos(file_get_contents('/etc/redhat-release'), 'CentOS Linux release 8.5.') === false) {
        exit("We expect this script to run only on CentOS 8.5\n");
    }
}

function is_virtualbox()
{
    return (strpos(shell_exec('dmidecode'), 'VirtualBox') !== false);
}

function process_commands($commands, $step_by_step)
{
    foreach ($commands as $c) {
        if ($c === null) {
            continue;
        }

        if (!is_array($c)) {
            $c = array('COMMAND', $c);
        }

        echo 'Executing ' . var_export($c, true) . "\n";

        switch ($c[0]) {
            case 'COMMAND':
                echo shell_exec($c[1] . ' 2>&1') . "\n";
                break;

            case 'REPLACE':
                if (!is_file($c[1])) {
                    show_error('Missing file, ' . $c[1]);
                    continue 2;
                }
                $contents = file_get_contents($c[1]);
                if ((strpos($contents, $c[2]) === false) && (empty($c[4]))) {
                    show_error('Missing string ' . $c[2] . ', in file, ' . $c[1]);
                    continue 2;
                }
                file_put_contents($c[1], str_replace($c[2], $c[3], $contents));
                echo 'Written ' . $c[1] . "\n";
                break;

            case 'PREG_REPLACE':
                if (!is_file($c[1])) {
                    show_error('Missing file, ' . $c[1]);
                    continue 2;
                }
                $contents = file_get_contents($c[1]);
                if (preg_match($c[2], $contents) == 0) {
                    show_error('Missing pattern ' . $c[2] . ', in file, ' . $c[1]);
                    continue 2;
                }
                file_put_contents($c[1], preg_replace($c[2], $c[3], $contents));
                echo 'Written ' . $c[1] . "\n";
                break;

            case 'APPEND':
                if (!is_file($c[1])) {
                    show_error('Missing file, ' . $c[1]);
                    continue 2;
                }
                $contents = file_get_contents($c[1]);
                file_put_contents($c[1], trim($contents) . "\n\n" . $c[2]);
                echo 'Written ' . $c[1] . "\n";
                break;

            case 'APPEND_OR_CREATE':
                if (is_file($c[1])) {
                    $contents = file_get_contents($c[1]);
                } else {
                    $contents = '';
                }
                file_put_contents($c[1], trim($contents) . "\n\n" . $c[2]);
                echo 'Written ' . $c[1] . "\n";
                break;

            case 'CREATE':
                file_put_contents($c[1], $c[2]);
                echo 'Written ' . $c[1] . "\n";
                break;

            case 'APPEND_AFTER':
                if (!is_file($c[1])) {
                    show_error('Missing file, ' . $c[1]);
                    continue 2;
                }
                $contents = file_get_contents($c[1]);
                $pos = strpos($contents, $c[2]);
                if ($pos === false) {
                    show_error('Missing string ' . $c[2] . ', in file, ' . $c[1]);
                    continue 2;
                }
                $pos += strlen($c[2]);
                file_put_contents($c[1], trim(substr($contents, 0, $pos)) . "\n" . $c[3] . "\n" . trim(substr($contents, $pos)));
                echo 'Written ' . $c[1] . "\n";
                break;
        }

        if ($step_by_step) {
            echo "(Press enter to continue or ctrl+c to exit script)\n";
            flush();
            $stdin = fopen('php://stdin', 'r');
            fgets($stdin);
            fclose($stdin);
        }
    }
}

function show_error($msg)
{
    echo 'ERROR: ' . $msg . "\n";

    echo "(Press enter to continue or ctrl+c to exit script)\n";
    flush();
    $stdin = fopen('php://stdin', 'r');
    fgets($stdin);
    fclose($stdin);
}
