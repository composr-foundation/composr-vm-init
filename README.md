# VM Initialization for Composr hosts

This project is sample code for initialising Composr VM hosts on CentOS 8.5. It should be considered illustrative not definitive. CentOS is no longer supported for one thing!

## Basic steps prior to running this script

1) Install Basic CentOS 8.5 VM onto a VPS
2) SSH into the server, `ssh root@<ip>`
3) Get networking working. On VirtualBox that may mean configuring a "Bridged Adapter" (let's you SSH in from outside the VirtualBox window). You can find the IP address via `ip addr show`
4) Update CentOS to a working mirror (now it is not supported):
```
cd /etc/yum.repos.d/
sudo sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
sudo sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
sudo yum update -y
```
5) `yum -y install php`
6) Exit from SSH, `exit`
7) Copy over your private clone of the composr-vm_init repository, `scp -r composr-vm_init root@<ip>:/`
8) SSH into the server, `ssh root@<ip>`
9) Change directory, `cd /composr-vm_init`
10) Run root init command, `php init_root.php --hostname=<hostname> --mysql-root-password=<mysql-root-password> --mysql-user-password=<mysql-user-password> --user-password=<user-password> --git-email=<git-email> --ssh-public-key="<ssh-public-key>"`
11) Exit from SSH, `exit`
12) Create a database dump, `mysqldump cms > ~/db.sql`
13) Copy over your database dump, `scp -r ~/db.sql cms@<ip>:~/`
14) SSH into the server, `ssh cms@<ip>`
15) Change directory, `cd /composr-vm_init`
16) Switch to new cms user, `su cms`
17) Run cms init command, `php init_user.php --mysql-user-password=<mysql-user-password> --repos=<git-repository> --git-email=<git-email> --ssh-public-key="<ssh-public-key>"`

